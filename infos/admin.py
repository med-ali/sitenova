from django.contrib import admin
from .models import About, Team, Contact

# Register your models here.
admin.site.register(About)
admin.site.register(Team)
admin.site.register(Contact)
