from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class About(models.Model):

    title = models.CharField(max_length=300)
    content = models.TextField(max_length = 10000)

    def __str__(self):
        return self.title 

class Contact(models.Model):

    phone = PhoneNumberField(null=False, blank=False, unique=True)
    email = models.EmailField(max_length=70,blank=True)
    facebook = models.URLField(max_length = 200)

class Team(models.Model):

    name = models.CharField(max_length = 50)
    photo = models.ImageField(upload_to='site_media')
    description = models.TextField(max_length=100)

    def __str__(self):
        return self.name
