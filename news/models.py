from django.db import models

# Create your models here.
class Article(models.Model):
    
    TYPE_SELECT = (
        ('Event', 'Event'),
        ('Video', 'Video'),
    )

    title = models.CharField(max_length = 100, blank = False)
    description = models.TextField(blank = False)
    media = models.ImageField(upload_to='site_media')
    Date = models.DateField()
    Category = models.CharField(max_length=11,choices=TYPE_SELECT)

    def __str__(self):
        return self.title