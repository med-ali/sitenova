from django.urls import path
from . import views

urlpatterns = [
    path('', views.responseform, name='post_demande'),
]

'''from django.conf.urls import url

from .forms import MyForm, Detailsform
from .views import FormWizardView

urlpatterns = [
    url(r'^$', FormWizardView.as_view([MyForm, Detailsform])),
]'''