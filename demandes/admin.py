from django.contrib import messages, admin
from .models import Demande
from .forms import MyForm
from django.utils.html import format_html
from django.core.mail import send_mail
from  sitenova import settings
# Register your models here.

def active_profil(modeladmin, request, queryset):
    queryset.update(is_actived = True)

def desactive_profil(modeladmin, request, queryset):
    queryset.update(is_actived = False)

desactive_profil.short_description = "Desactive Profil"
active_profil.short_description = "Active Profil"

def Send_Email(self, request, queryset):
    html_content = "Your Registration has been approved."
    for profile in queryset:
        send_mail(subject="Invite", message=html_content, 
                    from_email=settings.EMAIL_HOST_USER,
                    recipient_list=[profile.email_adress])  # use your email function here
def Send_Email_Disapproved(self, request, queryset):
        # the below can be modified according to your application.
        # queryset will hold the instances of your model
    for profile in queryset:
        html_content = "Our Apology, Your Registration has been Disapproved "
        send_mail(subject="Invite", message=html_content, 
                    from_email=settings.EMAIL_HOST_USER,
                    recipient_list=[profile.email_adress])  

Send_Email.short_description = "Send Activate Mail"
Send_Email_Disapproved.short_description = "Send Apology Mail"

class UserDemande(admin.ModelAdmin):

    form = MyForm
    list_display = ['id', 'name', 'last_name','email_adress', 'birthday', 'adress', "status", "is_actived", 'activation_url',]
    actions = [active_profil, desactive_profil, Send_Email,Send_Email_Disapproved]
    list_filter = ('is_actived', 'status', 'etat_civil')
    search_fields = ['name', 'email_adress']
    def get_readonly_fields(self, request, obj=None):
        # make all fields readonly
        readonly_fields = list(
            [field.name for field in self.opts.local_fields] +
            [field.name for field in self.opts.local_many_to_many]
        )
        list_display_links = readonly_fields.insert(0,'is_actived')
        if 'is_actived' in readonly_fields:
            readonly_fields.remove('is_actived')
        return readonly_fields
    
    def activation_url(self, obj):
        
        return format_html('<a href="http://127.0.0.1:8000/admin/demandes/demande/%s/change/">to: %s</a>' 
                            % (obj.id,obj.email_adress))

    activation_url.allow_tags = True


admin.site.register(Demande, UserDemande)
