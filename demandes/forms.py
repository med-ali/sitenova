from django import forms
from django_countries.fields import CountryField
from django.forms import ModelForm, Textarea
from . import models
from django.contrib.auth import get_user_model

User = get_user_model()
class DateInput(forms.DateInput):
    input_type = 'date' 
class MyForm(forms.ModelForm):
 
    class Meta:
        model = models.Demande
        fields = '__all__' #['status', 'name', 'last_name', 'birthday', 'adress', "etat_civil","country"]#'__all__' 
        widgets = {
            'is_actived': forms.HiddenInput(),
        }
        widgets = {
                'derniere_annee_de_scolarite_termine': DateInput(),
                'birthday': DateInput()
            }
        def save(self, commit=True):

            user = User.objects.create_user(
                **self.fields
            )
            return user







    '''STATUS_CHOICES = (
    (1, ("M.")),
    (2, ("Mlle")),
    (3, ("Mme")),
    )
    STATUS = (
    (1, ("Célibataire.")),
    (2, ("Marié(e)/Conjoint(e) de fait")),
    (3, ("Divorcé(e)/Séparé(e)")),
    (4, ("Veuf(ve)"))
    )
    status = forms.ChoiceField(choices = STATUS_CHOICES, label="Salutaion*", 
                                    initial='1', widget=forms.Select(), 
                                    required=True)
    first_name = forms.CharField(label='Prenom*', max_length=40, required = True)
    last_name = forms.CharField(label='Nom Famille*', max_length=40, required = True)
    birthday = forms.DateField ( label = 'Date De Naissance*', required= True)
    adress = forms.CharField(label='Adresse*', max_length=40, required = True)
    etat_civil = forms.ChoiceField(choices = STATUS, label="Etat Civil*", 
                                    initial='1', widget=forms.Select(), 
                                    required=True)
    country = CountryField(blank_label='select country').formfield(label='Country*')'''
