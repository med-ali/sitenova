
from django.shortcuts import render
from demandes.forms import MyForm
from django.shortcuts import render

'''from django.shortcuts import render
from formtools.wizard.views import SessionWizardView

class FormWizardView(SessionWizardView):
    template_name = "path/to/template"
    form_list = [MyForm, Detailsform]
    def done(self, form_list, **kwargs):
        return render(self.request, 'responseform.html', {
            'form_data': [form.cleaned_data for form in form_list],
        })'''

def responseform(request):

    form = MyForm(request.POST or None)

    if form.is_valid():
        form.save()
        
    context = {
        'form': form
    }
    return render(request, "responseform.html", context)
    

