import admin_notifications
from .models import Demande

def notification():
    count = Demande.objects.count()
    return 'You have {} new contacts <a href="/admin/demandes/">message</a>'.format(count)

admin_notifications.register(notification)