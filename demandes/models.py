from django.db import models
from django import forms
from django.forms import ModelForm, Textarea
import datetime
import uuid
from django_countries.fields import CountryField
from phonenumber_field.modelfields import PhoneNumberField

# Create your models here.
class Demande(models.Model):

    STATUS_CHOICES = (
        ("M.", "M."),
        ("Mlle", "Mlle"),
        ("Mme", "Mme"),
    )
    STATUS = (
        ("Célibataire.," ,"Célibataire."),
        ("Marié(e)/Conjoint(e) de fait", "Marié(e)/Conjoint(e) de fait"),
        ("Divorcé(e)/Séparé(e)", "Divorcé(e)/Séparé(e)"),
        ("Veuf(ve)", "Veuf(ve)")
    )
    STATUS_LEGAU = (
        ("---Statut Légal---," ,"---Statut Légal---"),
        ("Citoyen", "Citoyen"),
        ("Résident Permanent", "Résident Permanent"),
        ("Résident temporaire", "Résident temporaire"),
        ("Étudiant", "Étudiant"),
        ("Autres", "Autres")
    )
    LEVEL = (
        ("N'a pas terminé 2 ans d'enseignement secondaire", "N'a pas terminé 2 ans d'enseignement secondaire"),
        ("Deux ans d'enseignement secondaire terminé", "Deux ans d'enseignement secondaire terminé"),
        ("Trois ans d'enseignement secondaire terminé", "Trois ans d'enseignement secondaire terminé"),
        ("Quatre ans d'enseignement secondaire terminé", "Quatre ans d'enseignement secondaire terminé"),
        ("Certificat de formation professionnelle", "Certificat de formation professionnelle"),
        ("Baccalauréat","Baccalauréat"),
        ("Licence","Licence"),
        ("Premier cycle d'étude terminé","Premier cycle d'étude terminé"),
        ("Maitrise universitaire","Maitrise universitaire"),
        ("Diplôme d'etudes approfondies","Diplôme d'etudes approfondies"),
        ("Master","Master"),
        ("Doctorat", "Doctorat"),
    
    )

    DOM = (
        ("Administration et gestion", ""),
        ("Arts", "Arts"),
        ("Droit", "Droit"),
        ("Éducation", "Éducation"),
        ("Langues", "Langues" ),
        ("Santé", "Santé" ),
        ("Sciences humaines", "Sciences humaines" ),
        ("Sciences pures et appliquées" , "Sciences pures et appliquées"),
        ("Sciences sociales", "Sciences sociales" ),
        ("Technologie de l'information", "Technologie de l'information" ),
        ("Autres" , "Autres"),
    )
    NOTE = (
        ("9 à 11", "9 à 11"),
        ("11 à 13", "11 à 13"),
        ("13 à 15", "13 à 15"),
        ("15 et plus", "15 et plus"),
    )
    #user_id = models.UUIDField(primary_key=True, default=uuid.uuid4(), editable=False)
    is_actived = models.BooleanField(editable=True, default = False)
    status = models.CharField( max_length=5, choices=STATUS_CHOICES, default = '--------', blank = False)
    name = models.CharField(max_length = 50, default = '', blank = False)
    last_name = models.CharField(max_length = 50, default = '', blank = False)
    birthday = models.DateField (default=datetime.date.today, blank = False)
    adress = models.CharField( max_length=40, default='', blank = False)
    etat_civil = models.CharField(choices = STATUS, max_length = 50, default = '--------', blank = False)
    country = CountryField(blank_label='select country', blank = False, default = '')
    email_adress = models.EmailField(null = False,blank = False, default = '')
    tel = models.CharField(default = '', max_length=12, blank = False)
    country_actual =  CountryField(blank_label='select country', blank = False, default = '')
    plus_haut_Niveau_de_scolarite = models.CharField(choices = LEVEL, max_length = 100, default = '--------', blank = False)
    derniere_annee_de_scolarite_termine =  models.DateField (default=datetime.date.today, blank = False)
    domaine_etudes = models.CharField(choices = DOM, max_length = 100, default = '--------', blank = False)
    etablissements_Frequentes  =  models.CharField(max_length = 50, default = '', blank = False)
    moyenne_cumulative_dernier_diplome_obtenu = models.CharField(choices = NOTE, max_length = 100, default = '--------', blank = False)
    domaine_d_etudes_desire_au_canada = models.CharField(max_length = 50, default = '', blank = False)
    type_d_etudes = models.CharField(choices = (
                                        ("Formation professionnelle", "Formation professionnelle"),
                                        ("Diplôme d'études collégiales", "Diplôme d'études collégiales"),
                                        ("Diplôme universitaire du 1er cycle", "Diplôme universitaire du 1er cycle"),
                                        ("Diplôme universitaire du 2ème cycle", "Diplôme universitaire du 2ème cycle"),
                                        ("MBA, DESS, Doctorat", "MBA, DESS, Doctorat"),
                                        ("Ecole de langue", "Ecole de langue"),
                                        ("Autres", "Autres")
                                        ), max_length = 100, default = '--------', blank = False)
    date_de_debut_souhaite = models.CharField(
                                    choices = (
                                        ("Immédiat", "Immédiat"),
                                        ("3 Mois", "3 Mois"),
                                        ("6 Mois", "6 Mois"),
                                        ("9 Mois", "9 Mois"),
                                        ("1 ans", "1 ans"),
                                        ("+1 ans", "+1 ans"),
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)
    que_Comptez_vous_Faire_a_La_Fin = models.CharField(
                                    choices = (
                                        ("Retourner dans mon pays d'origine", "Retourner dans mon pays d'origine"),
                                        ("S'installer et travailler au Canada", "S'installer et travailler au Canada"),
                                        ("Travailler dans un autre pays que le Canada", "Travailler dans un autre pays que le Canada"),
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)

    montant_Disponible_Pour_Mes_etudes = models.CharField(
                                    choices = (
                                        ("Moins de 20 000$", "Moins de 20 000$"),
                                        ("De 20 000$ à 30 000$", "De 20 000$ à 30 000$"),
                                        ("De 30 000$ à 50 000$", "De 30 000$ à 50 000$"),
                                        ("Plus de 50 000$", "Plus de 50 000$")
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)
                                
    financement_Des_etudes_Et_Du_Sejour = models.CharField(
                                    choices = (
                                        ("Mon argent", "Moins de 20 000$"),
                                        ("Mes Parents", "Mes Parents"),
                                        ("Bourses", "Bourses"),
                                        ("Pret Bancaire", "Pret Bancaire")
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)

    valeur_De_Vos_Biens_Et_Ceux_De_Vos_Parents =  models.CharField(
                                    choices = (
                                        ("Moins de 100 000$", "Moins de 100 000$"),
                                        ("De 100 000$ à 250 000$", "De 100 000$ à 250 000$"),
                                        ("De 250 000$ à 500 000$", "De 250 000$ à 500 000$"),
                                        ("Plus de 500 000$", "Plus de 500 000$")
                                        ), 
                                     max_length = 100,
                                     default = '--------', 
                                     blank = False)
    
    total_Des_Vos_Revenus_Mensuels = models.CharField(
                                    choices = (
                                        ("Moins que 1 000$ / Mois", "Moins que 1 000$ / Mois"),
                                        ("De 1 000$ à 2 000$ / Mois", "De 1 000$ à 2 000$ / Mois"),
                                        ("De 2 000$ à 5 000$ / Mois", "De 2 000$ à 5 000$ / Mois"),
                                        ("Plus que 5 000$ / Mois", "Plus que 5 000$ / Mois")
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)
                                
    comment_Avez_vous_Entendu_Parler_De_Nos = models.CharField(
                                    choices = (
                                        ("Facebook", "Facebook"),
                                        ("Twitter", "Twitter"),
                                        ("Radio", "Radio"),
                                        ("Tv", "Tv")
                                        ), 
                                    max_length = 100,
                                     default = '--------', 
                                     blank = False)
                                     
    information_Supplementaire = models.TextField (max_length = 400,default = '', blank = True)
    check = models.BooleanField(blank = True, default=False)
